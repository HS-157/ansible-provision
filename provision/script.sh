#!/usr/bin/env bash

sudo pacman -Sy
sudo pacman -S --needed --noconfirm archlinux-keyring
sudo pacman -S --needed --noconfirm reflector
sudo reflector --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist
sudo pacman -S --needed --noconfirm python ansible termite-terminfo


# sudo pacman -Syu --noconfirm
# sudo pacman -S --needed --noconfirm docker python ansible termite-terminfo
