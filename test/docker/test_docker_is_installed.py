import pytest
import testinfra

host = "localhost"

def test_docker_is_installed(host):
    docker = host.package("docker")
    assert docker.is_installed
