import pytest
import testinfra

host = "localhost"

def test_tomcat_port_bindings(host):
    tomcat = host.docker("tomcat-sample")
    inspect = tomcat.inspect()
    assert inspect["HostConfig"]["PortBindings"] == {'8080/tcp': [{'HostIp': '0.0.0.0', 'HostPort': '8080'}]}
