import pytest
import testinfra

host = "localhost"

def test_docker_run_tomcat(host):
    tomcat = host.docker("tomcat-sample")
    assert tomcat.is_running
