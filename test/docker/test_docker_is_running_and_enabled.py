import pytest
import testinfra

host = "localhost"

def test_docker_is_running_and_enabled(host):
    docker = host.service("docker")
    assert docker.is_running
    assert docker.is_enabled
