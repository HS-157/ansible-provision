import pytest
import testinfra

host = "localhost"

def test_http_listen(host):
    assert host.socket("tcp://0.0.0.0:8080").is_listening
