# Ansible-provision

Approvisionnement d'une application Tomcat dans un conteneur Docker via Ansible.

## Dépendances

### Vagrant

Pour utilisation dans Vagrant :

* Virtualbox
* Vagrant
* Internet
* Espace disque - 10 Go (pour la VM)

### Local

Pour une utilisation en local :

* Docker
* Ansible
* Python
* Pydocker
* Pytest
* Testinfra
* Internet

## Exécution

### Vagrant

Vagrant va installer tout ce qu'il y a besoin (Ansible, Docker, Pytest...) et lancer le playbook Ansible pour la construction de l'image, l'exécution du conteneur et lancer les tests.

~~~bash
vagrant up
~~~

Pour lancer à la main le playbook Ansible :

~~~bash
vagrant ssh
cd /vagrant
ansible-playbook -i inventory ansible-provision.yml
~~~

### Local

Pour lancer directement en local :

~~~bash
ansible-playbook -i inventory ansible-provision.yml
~~~

### Test

Il est possible d'exécuter directement les tests via Pytest.

#### Vagrant

~~~bash
vagrant ssh
cd /vagrant
sudo pytest
~~~

#### Local

~~~bash
sudo pytest
~~~

## Dossiers

Description des différents dossiers.

### docker

Fichiers nécessaire pour la construction de l'image Docker :

* Dockerfile - Fichier de construction de l'image Docker
* sample.war - Application Tomcat

### provision

Fichiers nécessaire pour l'approvisionnement de Vagrant :

* script.sh - Approvisionnement de base pour installer Ansible
* playbook.yml - Approvisionnement avancé pour installer Docker et différents dépenances

### roles

Dossier contenant les différents rôles Ansible :

* docker - Rôle pour la construction de l'image et l'exécution du conteneur
* test - Rôle qui lance les différents tests via Pytest

### test

Dossier contenant les différents tests pour le conteneur et l'application

* application - Tests pour l'application Tomcat
* docker - Test pour le conteneur Docker


## Ansible

### docker

* docker_image - Création d'une image Docker à partir d'un Dockerfile

Source :
* https://docs.ansible.com/ansible/latest/modules/docker_image_module.html

* docker_container - Exécution d'un conteneur à partir d'une image spécifique

Source :
* https://docs.ansible.com/ansible/latest/modules/docker_container_module.html

### test

* command - Exécution de la commande Pytest avec un test spécifique
  * register - Permet d'enregistrer la sortie de la commande
  * failed_when - Permet de tester la condition d'erreur et d'avoir un message personnalisé

Source :
* https://docs.ansible.com/ansible/latest/modules/command_module.html#command-module
* https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#registering-variables
* https://docs.ansible.com/ansible/latest/user_guide/playbooks_error_handling.html#controlling-what-defines-failure


## Dockerfile

* FROM - Utilisation d'une image de base
* MAINTAINER - Mainteneur de l'image
* COPY - Copie de l'application dans l'image

## Divers

Utilisation de sudo :

* `sudo` est utiliser pour la commande `docker`, soit pour lancer le conteneur (via Ansible) ou pour les tests (via Testinfra) car l'utilisateur courrant n'est pas rajouté au groupe docker.

Source : 

* https://wiki.archlinux.org/index.php/Docker#Installation
