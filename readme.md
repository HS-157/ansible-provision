# Ansible-provision

Provisioning a Tomcat application in a Docker container via Ansible.

[**Version en français du Readme**](readme_fr.md)

## Dependencies

### Vagrant

For use in Vagrant :

* Virtualbox
* Vagrant
* Internet
* Disk space - 10 GB (for the VM)

### Local

For use locally :

* Docker
* Ansible
* Python
* Pydocker
* Pytest
* Testinfra
* Internet

## Execution

### Vagrant

Vagrant will install everything it needs (Ansible, Docker, Pytest...) and launch the Ansible playbook to build the image, run the container and tests.

~~~bash
vagrant up
~~~

To hand launch the Ansible Playbook :

~~~bash
vagrant ssh
cd /vagrant
ansible-playbook -i inventory ansible-provision.yml
~~~

### Local

To launch directly in local :

~~~bash
ansible-playbook -i inventory ansible-provision.yml
~~~

### Test

It is possible to run the tests directly via Pytest.

#### Vagrant

~~~bash
vagrant ssh
cd /vagrant
sudo pytest
~~~

#### Local

~~~bash
sudo pytest
~~~

## Folders

Description of the different files.

### docker

Files needed to build the Docker image :

* Dockerfile - Docker image construction file
* sample.war - Tomcat Application

### provision

Files needed for the provisioning of Vagrant :

* script.sh - Base provision for installation Ansible
* playbook.yml - Advanced provisioning to install Docker and various outbuildings

### roles

File containing the different Ansible roles

* docker - Role for the image construction and execution of the container
* test - Role that launches the different tests via Pytest

### test

Folders containing the different tests for the container and the application

* application - Tests for Tomcat application
* docker - Test for Docker Container


## Ansible

### docker

* docker_image - Creating a Docker image from a Dockerfile

Source :
* https://docs.ansible.com/ansible/latest/modules/docker_image_module.html

* docker_container - Execution of a container from a specific image

Source :
* https://docs.ansible.com/ansible/latest/modules/docker_container_module.html

### test

* command - Executing the Pytest command with a specific test
  * register - Allows to save the output of the command
  * failed_when - Allows to test the error condition and to have a customized message

Source :
* https://docs.ansible.com/ansible/latest/modules/command_module.html#command-module
* https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#registering-variables
* https://docs.ansible.com/ansible/latest/user_guide/playbooks_error_handling.html#controlling-what-defines-failure


## Dockerfile

* FROM - Using a base image
* MAINTAINER - Image Maintainer 
* COPY - Copying the application into the image

## Miscellaneous

Use of sudo :

* `sudo` is used for the `docker` command, either to run the container (via Ansible) or for testing (via Testinfra) because the current user is not added to the docker group.

Source : 

* https://wiki.archlinux.org/index.php/Docker#Installation
